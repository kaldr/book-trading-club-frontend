import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import MockAdapter from 'axios-mock-adapter';

import axios from '../../axios';
import * as actions from './initialState';
import * as types from '../actionTypes';

const mockApi = new MockAdapter(axios);

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('initial state sync action creators', () => {
  test('set username', () => {
    const username = 'username';
    const expectedAction = {
      type: types.SET_USERNAME,
      username,
    };
    expect(actions.setUsername(username)).toEqual(expectedAction);
  });

  test('recieve books', () => {
    const books = [{id: '1'}, {id: '2'}];
    const expectedAction = {
      type: types.RECIEVE_BOOKS,
      books,
    };
    expect(actions.recieveBooks(books)).toEqual(expectedAction);
  });

  test('set initial state error', () => {
    const error = 'error';
    const expectedAction = {
      type: types.SET_INITIAL_STATE_ERROR,
      error,
    };
    expect(actions.setInitialStateError(error)).toEqual(expectedAction);
  });

  test('set initial state loading', () => {
    const loading = false;
    const expectedAction = {
      type: types.SET_INITIAL_STATE_LOADING,
      loading,
    };
    expect(actions.setInitialStateLoading(loading)).toEqual(expectedAction);
  });
});

describe('initial state async action creators', () => {
  test('successful initial state handler', async () => {
    const username = 'username';
    const books = [{id: '1'}, {id: '2'}];
    mockApi.onGet('/api/initstate').reply(200, {
      username,
      books,
    });

    const store = mockStore({});
    await store.dispatch(actions.initialStateHandler());
    expect(store.getActions()).toEqual([
      actions.setInitialStateLoading(false),
      actions.setUsername(username),
      actions.recieveBooks(books),
    ]);
  });

  test('failure initial state handler', async () => {
    mockApi.onGet('/api/initstate').reply(403);

    const store = mockStore({});
    await store.dispatch(actions.initialStateHandler());
    expect(store.getActions()).toEqual([
      actions.setInitialStateError('Request failed with status code 403'),
    ]);
  });
});