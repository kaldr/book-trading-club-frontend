import {batch} from 'react-redux';

import axios from '../../axios';
import {
  SET_USERNAME,
  RECIEVE_BOOKS,
  SET_INITIAL_STATE_ERROR,
  SET_INITIAL_STATE_LOADING,
} from '../actionTypes';

export const setUsername = username => ({
  type: SET_USERNAME,
  username,
});

export const recieveBooks = books => ({
  type: RECIEVE_BOOKS,
  books,
});

export const setInitialStateError = error => ({
  type: SET_INITIAL_STATE_ERROR,
  error,
});

export const setInitialStateLoading = loading => ({
  type: SET_INITIAL_STATE_LOADING,
  loading,
});

export const initialStateHandler = () => async dispatch => {
  try {
    const initialState = await axios.get('/api/initstate');
    const {username, books} = initialState.data;
    batch(() => {
      dispatch(setInitialStateLoading(false));
      dispatch(setUsername(username));
      dispatch(recieveBooks(books));
    });
  } catch (error) {
    dispatch(setInitialStateError(error.message));
  }
};