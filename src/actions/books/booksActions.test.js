import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import MockAdapter from 'axios-mock-adapter';

import axios from '../../axios';
import * as actions from './booksActions';
import * as types from '../actionTypes';

const mockApi = new MockAdapter(axios);

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('books sync action creators', () => {
  test('trade request failure', () => {
    const error = 'error';
    const expectedAction = {
      type: types.SET_TRADE_REQUEST_FAILURE,
      error,
    };
    expect(actions.setTradeRequestFailure(error)).toEqual(expectedAction);
  });

  test('trade request status', () => {
    const status = 'status';
    const bookId = 'bookId';
    const expectedAction = {
      type: types.SET_TRADE_REQUEST_STATUS,
      status,
      bookId,
    };
    expect(actions.setTradeRequestStatus(status, bookId)).toEqual(expectedAction);
  });

  test('add new trade', () => {
    const book = {id: '1'};
    const expectedAction = {
      type: types.ADD_NEW_TRADE,
      book,
    };
    expect(actions.addNewTrade(book)).toEqual(expectedAction);
  });
});

describe('books async action creators', () => {
  test('successful trade', async () => {
    const book = {id: 'id', accepted: false};
    const store = mockStore({});
    mockApi.onPost('/api/trades').reply(200);

    await store.dispatch(actions.tradeHandler(book));
    expect(store.getActions()).toEqual([
      actions.setTradeRequestStatus('inProcess', book.id),
      actions.setTradeRequestStatus('done', book.id),
      actions.addNewTrade(book),
    ]);
  });

  test('trade failure', async () => {
    const book = {id: 'id'};
    const store = mockStore({});
    mockApi.onPost('/api/trades').reply(403);

    await store.dispatch(actions.tradeHandler(book));
    expect(store.getActions()).toEqual([
      actions.setTradeRequestStatus('inProcess', book.id),
      actions.setTradeRequestFailure('Request failed with status code 403'),
      actions.setTradeRequestStatus('waitForClick', book.id),
    ]);
  });
});