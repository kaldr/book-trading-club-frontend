import {batch} from 'react-redux';

import axios from '../../axios';
import {
  SET_TRADE_REQUEST_STATUS,
  SET_TRADE_REQUEST_FAILURE,
  ADD_NEW_TRADE,
} from '../actionTypes';

export const setTradeRequestFailure = error => ({
  type: SET_TRADE_REQUEST_FAILURE,
  error,
});

export const setTradeRequestStatus = (status, bookId) => ({
  type: SET_TRADE_REQUEST_STATUS,
  status,
  bookId,
});

export const addNewTrade = book => ({
  type: ADD_NEW_TRADE,
  book,
});

export const tradeHandler = book => async dispatch => {
  try {
    dispatch(setTradeRequestStatus('inProcess', book.id));
    
    const {userBookId} = book;
    await axios.post('/api/trades', {userBookId});
    
    book = {...book, accepted: false};
    batch(() => {
      dispatch(setTradeRequestStatus('done', book.id));
      dispatch(addNewTrade(book));
    });
  } catch (err) {
    batch(() => {
      dispatch(setTradeRequestFailure(err.message));
      dispatch(setTradeRequestStatus('waitForClick', book.id));
    });
  }
};