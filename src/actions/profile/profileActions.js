import {batch} from 'react-redux';

import axios from '../../axios';
import {
  SET_PROFILE_SAVE_STATUS,
  SET_PROFILE_SAVE_FAILURE,
  SET_EDITING_STATUS,
  SET_TEMP_USER_INFO,
  SET_USER_INFO,
  SET_TRADE_BUTTONS_STATUS,
  SET_TRADE_ACCEPT,
  SET_TRADE_CANCEL,
  SET_PROFILE_INITIAL_STATE_ERROR,
  SET_PROFILE_INITIAL_STATE_LOADING,
  SET_PROFILE_TRADES,
} from '../actionTypes';

export const setProfileSaveStatus = saveStatus => ({
  type: SET_PROFILE_SAVE_STATUS,
  saveStatus,
});

export const setProfileSaveFailure = error => ({
  type: SET_PROFILE_SAVE_FAILURE,
  error,
});

export const setEditingStatus = editing => ({
  type: SET_EDITING_STATUS,
  editing,
});

export const setTempUserInfo = info => ({
  type: SET_TEMP_USER_INFO,
  info,
});

export const setUserInfo = info => ({
  type: SET_USER_INFO,
  info,
});

export const setTradeButtonsStatus = (tradeId, status) => ({
  type: SET_TRADE_BUTTONS_STATUS,
  tradeId,
  status,
});

export const setTradeAccept = tradeId => ({
  type: SET_TRADE_ACCEPT,
  tradeId,
});

export const setTradeCancel = tradeId => ({
  type: SET_TRADE_CANCEL,
  tradeId,
});

export const setProfileInitialStateError = error => ({
  type: SET_PROFILE_INITIAL_STATE_ERROR,
  error,
});

export const setProfileInitialStateLoading = loading => ({
  type: SET_PROFILE_INITIAL_STATE_LOADING,
  loading,
});

export const setProfileTrades = trades => ({
  type: SET_PROFILE_TRADES,
  trades,
});

export const profileInitialStateHandler = () => async dispatch => {
  try {
    const initialState = await axios.get('/api/profile');
    const {userInfo, userTrades, tradesForUser} = initialState.data;
    batch(() => {
      dispatch(setProfileInitialStateLoading(false));
      dispatch(setTempUserInfo(userInfo));
      dispatch(setUserInfo(userInfo));
      dispatch(setProfileTrades({userTrades, tradesForUser}));
    });
  } catch (error) {
    dispatch(setProfileInitialStateError(error.message));
  }
};

export const saveHandler = userInfo => async dispatch => {
  dispatch(setProfileSaveStatus('inProcess'));
  try {
    await axios.put('/api/profile', userInfo);
    batch(() => {
      dispatch(setUserInfo(userInfo));
      dispatch(setEditingStatus(false));
      dispatch(setProfileSaveStatus('waitForClick'));
    });
  } catch (err) {
    batch(() => {
      dispatch(setProfileSaveStatus('waitForClick'));
      dispatch(setProfileSaveFailure(err.message));
    });
  }
};

export const cancelEdit = () => async (dispatch, getState) => {
  const {profile} = getState();
  batch(() => {
    dispatch(setTempUserInfo(profile.userInfo));
    dispatch(setEditingStatus(false));
  });
};

export const acceptTradeRequset = tradeId => async dispatch => {
  dispatch(setTradeButtonsStatus(tradeId, 'inProcess'));
  try {
    await axios.put(`/api/trades/${tradeId}/accept`);
    dispatch(setTradeAccept(tradeId));
  } catch (error) {}
};

export const cancelTradeRequest = tradeId => async dispatch => {
  dispatch(setTradeButtonsStatus(tradeId, 'inProcess'));
  try {
    await axios.put(`/api/trades/${tradeId}/cancel`);
    dispatch(setTradeCancel(tradeId));
  } catch (error) {}
};