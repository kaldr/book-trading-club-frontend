import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import MockAdapter from 'axios-mock-adapter';

import axios from '../../axios';
import * as actions from './profileActions';
import * as types from '../actionTypes';

const mockApi = new MockAdapter(axios);

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('profile sync action creators', () => {
  test('set profile save status', () => {
    const saveStatus = 'inProcess';
    const expectedAction = {
      type: types.SET_PROFILE_SAVE_STATUS,
      saveStatus,
    };
    expect(actions.setProfileSaveStatus(saveStatus)).toEqual(expectedAction);
  });

  test('profile save failure', () => {
    const error = 'error';
    const expectedAction = {
      type: types.SET_PROFILE_SAVE_FAILURE,
      error,
    };
    expect(actions.setProfileSaveFailure(error)).toEqual(expectedAction);
  });

  test('editing status', () => {
    const editing = false;
    const expectedAction = {
      type: types.SET_EDITING_STATUS,
      editing,
    };
    expect(actions.setEditingStatus(editing)).toEqual(expectedAction);
  });

  test('set temp user info', () => {
    const info = 'info';
    const expectedAction = {
      type: types.SET_TEMP_USER_INFO,
      info,
    };
    expect(actions.setTempUserInfo(info)).toEqual(expectedAction);
  });

  test('set trade button status', () => {
    const tradeId = 'id';
    const status = 'inProcess';
    const expectedAction = {
      type: types.SET_TRADE_BUTTONS_STATUS,
      tradeId,
      status,
    };
    expect(actions.setTradeButtonsStatus(tradeId, status)).toEqual(expectedAction);
  });

  test('set trade accept', () => {
    const tradeId = 'id';
    const expectedAction = {
      type: types.SET_TRADE_ACCEPT,
      tradeId,
    };
    expect(actions.setTradeAccept(tradeId)).toEqual(expectedAction);
  });

  test('set trade cancel', () => {
    const tradeId = 'id';
    const expectedAction = {
      type: types.SET_TRADE_CANCEL,
      tradeId,
    };
    expect(actions.setTradeCancel(tradeId)).toEqual(expectedAction);
  });

  test('set profile initial state error', () => {
    const error = 'error';
    const expectedAction = {
      type: types.SET_PROFILE_INITIAL_STATE_ERROR,
      error,
    };
    expect(actions.setProfileInitialStateError(error)).toEqual(expectedAction);
  });

  test('set profile initial state loading', () => {
    const loading = false;
    const expectedAction = {
      type: types.SET_PROFILE_INITIAL_STATE_LOADING,
      loading,
    };
    expect(actions.setProfileInitialStateLoading(loading)).toEqual(expectedAction);
  });

  test('set profile trades', () => {
    const trades = [{id: '1'}, {id: '2'}];
    const expectedAction = {
      type: types.SET_PROFILE_TRADES,
      trades,
    };
    expect(actions.setProfileTrades(trades)).toEqual(expectedAction);
  });
});

describe('profile async action creators', () => {
  test('successful profile save', async () => {
    const userInfo = {info: 'info'};
    const store = mockStore({});
    mockApi.onPut('/api/profile').reply(200);

    await store.dispatch(actions.saveHandler(userInfo));
    expect(store.getActions()).toEqual([
      actions.setProfileSaveStatus('inProcess'),
      actions.setUserInfo(userInfo),
      actions.setEditingStatus(false),
      actions.setProfileSaveStatus('waitForClick'),
    ]);
  });

  test('profile save failure', async () => {
    const userInfo = {info: 'info'};
    const store = mockStore({});
    mockApi.onPut('/api/profile').reply(500);

    await store.dispatch(actions.saveHandler(userInfo));
    expect(store.getActions()).toEqual([
      actions.setProfileSaveStatus('inProcess'),
      actions.setProfileSaveStatus('waitForClick'),
      actions.setProfileSaveFailure('Request failed with status code 500'),
    ]);
  });

  test('cancel edit', async () => {
    const profile = {userInfo: 'userInfo'};
    const store = mockStore({profile});
    await store.dispatch(actions.cancelEdit());
    expect(store.getActions()).toEqual([
      actions.setTempUserInfo(profile.userInfo),
      actions.setEditingStatus(false),
    ]);
  });

  test('accept trade request', async () => {
    const tradeId = 'tradeId';
    const store = mockStore({});
    // /api/trades/:tradeId/accept
    mockApi.onPut(/\/api\/trades\/.*\/accept/).reply(200);

    await store.dispatch(actions.acceptTradeRequset(tradeId));
    expect(store.getActions()).toEqual([
      actions.setTradeButtonsStatus(tradeId, 'inProcess'),
      actions.setTradeAccept(tradeId),
    ]);
  });

  test('cancel trade request', async () => {
    const tradeId = 'tradeId';
    const store = mockStore({});
    // /api/trades/:tradeId/cancel
    mockApi.onPut(/\/api\/trades\/.*\/cancel/).reply(200);

    await store.dispatch(actions.cancelTradeRequest(tradeId));
    expect(store.getActions()).toEqual([
      actions.setTradeButtonsStatus(tradeId, 'inProcess'),
      actions.setTradeCancel(tradeId),
    ]);
  });

  test('successful profile initial state handler', async () => {
    const userInfo = {fullname: 'fullname'};
    const userTrades = [{id: '1'}, {id: '2'}];
    const tradesForUser = [{id: '3'}];

    mockApi.onGet('/api/profile')
      .reply(200, {userInfo, userTrades, tradesForUser});

    const store = mockStore({});
    await store.dispatch(actions.profileInitialStateHandler());

    expect(store.getActions()).toEqual([
      actions.setProfileInitialStateLoading(false),
      actions.setTempUserInfo(userInfo),
      actions.setUserInfo(userInfo),
      actions.setProfileTrades({userTrades, tradesForUser}),
    ]);
  });

  test('failure profile initial state handler', async () => {
    mockApi.onGet('/api/profile').reply(403);

    const store = mockStore({});
    await store.dispatch(actions.profileInitialStateHandler());

    expect(store.getActions()).toEqual([
      actions.setProfileInitialStateError('Request failed with status code 403'),
    ]);
  });
});