import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import MockAdapter from 'axios-mock-adapter';

import axios, {rawAxios} from '../../axios';
import {searchedBooksWithHelloPhraseWithThumbnail} from '../../devUtils/mockApi';
import * as actions from './addBookActions';

const mock = new MockAdapter(axios);

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('add book async action creators', () => {
  afterEach(() => {
    mock.reset();
  });

  test('get searched books', async () => {
    const url = `https://www.googleapis.com/books/v1/volumes?q=hello&projection=lite&maxResults=5&prettyPrint=false`;
    const store = mockStore({
      username: 'username',
      addBookState: {
        textToSearch: 'hello',
      },
    });
    const books = [{"id":"fP-mCwAAQBAJ","title":"Hello!","thumbnail":"https://books.google.com/books/content?id=fP-mCwAAQBAJ&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api","owner":"username","addButtonState":"waitForClick"},{"id":"pwRcrgEACAAJ","title":"Hello?","thumbnail":"https://books.google.com/books/content?id=pwRcrgEACAAJ&printsec=frontcover&img=1&zoom=1&source=gbs_api","owner":"username","addButtonState":"waitForClick"},{"id":"AKQd0WfQaw0C","title":"Hello, Hello Brazil","thumbnail":"https://books.google.com/books/content?id=AKQd0WfQaw0C&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api","owner":"username","addButtonState":"waitForClick"},{"id":"3u3WwWg82nQC","title":"Hello! Good-bye!","thumbnail":"https://books.google.com/books/content?id=3u3WwWg82nQC&printsec=frontcover&img=1&zoom=1&source=gbs_api","owner":"username","addButtonState":"waitForClick"},{"id":"AhBNeNVzMGIC","title":"Hello","thumbnail":"https://books.google.com/books/content?id=AhBNeNVzMGIC&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api","owner":"username","addButtonState":"waitForClick"}];

    const mockRawAxios = new MockAdapter(rawAxios);
    searchedBooksWithHelloPhraseWithThumbnail(mockRawAxios);
    await actions.getSearchedBooks(store.dispatch, store.getState, url);
    
    expect(store.getActions()).toEqual([
      actions.setIsSearching(true),
      actions.recieveSearchedBooks(books),
    ]);
    mockRawAxios.reset();
  });

  test('same textToSearch', () => {
    const getSearchedBooks = jest.fn();
    const store = mockStore({
      username: 'username',
      addBookState: {
        textToSearch: 'hello',
      },
    });

    const textToSearch = 'hello';

    store.dispatch(actions.textToSearchChangeHandling(textToSearch, getSearchedBooks));
    expect(store.getActions()).toEqual([
      actions.setTextToSearch(textToSearch),
    ]);
    expect(getSearchedBooks.mock.calls.length).toBe(0);
  });

  test('empty textToSearch', () => {
    const getSearchedBooks = jest.fn();
    const store = mockStore({
      username: 'username',
      addBookState: {
        textToSearch: 'hello',
      },
    });

    const textToSearch = '';

    store.dispatch(actions.textToSearchChangeHandling(textToSearch, getSearchedBooks));
    expect(store.getActions()).toEqual([
      actions.setTextToSearch(textToSearch),
      actions.recieveSearchedBooks([]),
    ]);
    expect(getSearchedBooks.mock.calls.length).toBe(0);
  });

  test('new textToSearch', () => {
    const getSearchedBooks = jest.fn();
    const store = mockStore({
      username: 'username',
      addBookState: {
        textToSearch: 'hello',
      },
    });

    const textToSearch = 'new';

    store.dispatch(actions.textToSearchChangeHandling(textToSearch, getSearchedBooks));
    expect(store.getActions()).toEqual([
      actions.setTextToSearch(textToSearch),
    ]);
    expect(getSearchedBooks.mock.calls.length).toBe(1);
  });

  test('close handler', () => {
    const store = mockStore({});

    store.dispatch(actions.closeHandler());
    expect(store.getActions()).toEqual([
      actions.setTextToSearch(''),
      actions.recieveSearchedBooks([]),
    ]);
  });

  test('add book handler', async () => {
    const book = {id: 'id'};
    const store = mockStore({});
    mock.onPost('/api/books').reply(200);

    await store.dispatch(actions.addBookHandler(book));
    expect(store.getActions()).toEqual([
      actions.changeAddButtonState('inProcess', book.id),
      actions.changeAddButtonState('done', book.id),
      actions.addBook(book),
    ]);
  });
});