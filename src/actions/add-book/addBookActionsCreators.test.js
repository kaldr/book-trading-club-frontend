import * as actions from './addBookActions';
import * as types from '../actionTypes';

describe('add book sync action creators', () => {
  test('should create an action to change AddButton state', () => {
    const addButtonState = 'done';
    const id = 'id';
    const expectedAction = {
      type: types.CHANGE_ADD_BUTTON_STATE,
      addButtonState,
      id,
    };
    expect(actions.changeAddButtonState(addButtonState, id)).toEqual(expectedAction);
  });

  test('should create an action to add a new book', () => {
    const book = {name: 'new book'};
    const expectedAction = {
      type: types.ADD_BOOK,
      book,
    };
    expect(actions.addBook(book)).toEqual(expectedAction);
  });

  test('should create an action to recieve searched books', () => {
    const books = [{name: 'new book'}];
    const expectedAction = {
      type: types.RECIEVE_SEARCHED_BOOKS,
      books,
    };
    expect(actions.recieveSearchedBooks(books)).toEqual(expectedAction);
  });

  test('should create an action to fail recieve searched books', () => {
    const error = {message: 'error'};
    const expectedAction = {
      type: types.SEARCHED_BOOKS_FAILURE,
      error,
    };
    expect(actions.searchedBooksFailure(error)).toEqual(expectedAction);
  });

  test('should create an action to set text to search', () => {
    const textToSearch = 'textToSearch';
    const expectedAction = {
      type: types.SET_TEXT_TO_SEARCH,
      textToSearch,
    };
    expect(actions.setTextToSearch(textToSearch)).toEqual(expectedAction);
  });

  test('should create an action to set is searching state', () => {
    const isSearching = true;
    const expectedAction = {
      type: types.SET_IS_SEARCHING,
      isSearching,
    };
    expect(actions.setIsSearching(isSearching)).toEqual(expectedAction);
  });
});