import debounce from 'lodash/debounce';
import {batch} from 'react-redux';

import axios, {rawAxios} from '../../axios';
import {
  RECIEVE_SEARCHED_BOOKS,
  SEARCHED_BOOKS_FAILURE,
  SET_TEXT_TO_SEARCH,
  SET_IS_SEARCHING,
  ADD_BOOK,
  CHANGE_ADD_BUTTON_STATE,
} from '../actionTypes';

export function searchUrl(text) {
  text = text.trim().split(' ').join('+');
  return `https://www.googleapis.com/books/v1/volumes?q=${text}&projection=lite&maxResults=5&prettyPrint=false`;
}

export const changeAddButtonState = (addButtonState, id) => ({
  type: CHANGE_ADD_BUTTON_STATE,
  addButtonState,
  id,
});

export const addBook = book => ({
  type: ADD_BOOK,
  book,
});

export const recieveSearchedBooks = books => ({
  type: RECIEVE_SEARCHED_BOOKS,
  books,
});

export const searchedBooksFailure = error => ({
  type: SEARCHED_BOOKS_FAILURE,
  error,
});

export const setTextToSearch = textToSearch => ({
  type: SET_TEXT_TO_SEARCH,
  textToSearch,
});

export const setIsSearching = isSearching => ({
  type: SET_IS_SEARCHING,
  isSearching,
});

let cancelToken = axios.CancelToken.source();

export const getSearchedBooks = async (dispatch, getState, url) => {
  cancelToken.cancel();
  dispatch(setIsSearching(true));

  try {
    cancelToken = axios.CancelToken.source();
    const response = await rawAxios.get(url, {cancelToken: cancelToken.token});
    
    const {username, addBookState} = getState();
    if (!addBookState.textToSearch) {
      return dispatch(setIsSearching(false));
    }

    const books = response.data.items.map(book => {
      const thumbnail = book.volumeInfo.imageLinks ? 
        book.volumeInfo.imageLinks.thumbnail.replace('http://', 'https://') : undefined;
      return {
        id: book.id,
        title: book.volumeInfo.title,
        thumbnail,
        owner: username,
        addButtonState: 'waitForClick',
      }
    });

    dispatch(recieveSearchedBooks(books));
  } catch (err) {
    if (axios.isCancel(err)) {
      return dispatch(setIsSearching(false));
    }
    
    dispatch(searchedBooksFailure(err.message));
  }
};

export const getSearchedBooksDebounced = debounce(getSearchedBooks, 300);

export const textToSearchChangeHandling = (textToSearch, getSearchedBooks) => (dispatch, getState) => {
  const {addBookState} = getState();
  
  dispatch(setTextToSearch(textToSearch));
  
  if (addBookState.textToSearch.trim() === textToSearch.trim()) {
    return;
  }
  if (textToSearch.trim() === '') {
    return dispatch(recieveSearchedBooks([]));
  }
  
  getSearchedBooks(dispatch, getState, searchUrl(textToSearch));
};

export const closeHandler = () => dispatch => {
  batch(() => {
    dispatch(setTextToSearch(''));
    dispatch(recieveSearchedBooks([]));
  });
};

export const addBookHandler = book => async dispatch => {
  dispatch(changeAddButtonState('inProcess', book.id));
  try {
    await axios.post('/api/books', {...book});
    batch(() => {
      dispatch(changeAddButtonState('done', book.id));
      dispatch(addBook(book));
    });
  } catch (err) {
    dispatch(searchedBooksFailure(err.message));
  }
};