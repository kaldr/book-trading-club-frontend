import {batch} from 'react-redux';

import axios from '../../axios';
import {
  SIGN_IN_SET_FIELD,
  CHANGE_SIGN_IN_BUTTON_STATUS,
  SET_SIGN_IN_ERROR,
} from '../actionTypes';
import {setUsername} from '../../actions/username/usernameActions';

export const signInSetField = field => ({
  type: SIGN_IN_SET_FIELD,
  field,
});

export const changeSignInButtonStatus = signInButtonStatus => ({
  type: CHANGE_SIGN_IN_BUTTON_STATUS,
  signInButtonStatus,
});

export const setSignInError = error => ({
  type: SET_SIGN_IN_ERROR,
  error,
});

export const signInHandler = () => async (dispatch, getState) => {
  try {
    dispatch(changeSignInButtonStatus('inProcess'));
    const {username, password} = getState().signIn;
    await axios.post('/api/signin', {username, password});
    batch(() => {
      dispatch(setUsername(username));
      dispatch(changeSignInButtonStatus('waitForClick'));
    });
  } catch (error) {
    const errorMessage = error.response ? (error.response.data ? error.response.data.message : error.message) : error.message;
    batch(() => {
      dispatch(setSignInError(errorMessage));
      dispatch(changeSignInButtonStatus('waitForClick'));
    });
  }
};