import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import MockAdapter from 'axios-mock-adapter';

import axios from '../../axios';
import * as actions from './signInActions';
import {setUsername} from '../../actions/username/usernameActions';
import * as types from '../actionTypes';

const mockApi = new MockAdapter(axios);

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('sign in sync action creators', () => {
  test('sign in set field', () => {
    const field = {username: 'username'};
    const expectedActions = {
      type: types.SIGN_IN_SET_FIELD,
      field,
    };
    expect(actions.signInSetField(field)).toEqual(expectedActions);
  });

  test('change sign in button status', () => {
    const signInButtonStatus = 'done';
    const expectedActions = {
      type: types.CHANGE_SIGN_IN_BUTTON_STATUS,
      signInButtonStatus,
    };
    expect(actions.changeSignInButtonStatus(signInButtonStatus)).toEqual(expectedActions);
  });

  test('set sign in error', () => {
    const error = 'error';
    const expectedActions = {
      type: types.SET_SIGN_IN_ERROR,
      error,
    };
    expect(actions.setSignInError(error)).toEqual(expectedActions);
  });
});

describe('sign in async action creators', () => {
  test('successful sign in', async () => {
    const username = 'username';
    const password = 'password';
    const store = mockStore({
      signIn: {username, password},
    });
    mockApi.onPost('/api/signin').reply(200);

    await store.dispatch(actions.signInHandler());
    expect(store.getActions()).toEqual([
      actions.changeSignInButtonStatus('inProcess'),
      setUsername(username),
      actions.changeSignInButtonStatus('done'),
    ]);
  });

  test('failure sign in', async () => {
    const username = 'username';
    const password = 'password';
    const store = mockStore({
      signIn: {username, password},
    });
    mockApi.onPost('/api/signin').reply(500);

    await store.dispatch(actions.signInHandler());
    expect(store.getActions()).toEqual([
      actions.changeSignInButtonStatus('inProcess'),
      actions.setSignInError('Request failed with status code 500'),
      actions.changeSignInButtonStatus('waitForClick'),
    ]);
  });
});