import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';

import Root from './components/root/Root';
import configureStore from './store/configureStore';
import registerServiceWorker from './registerServiceWorker';

let preloadedState;

if (process.env.NODE_ENV !== 'production') {
  //preloadedState = require('./devUtils/preloadedState.json');
  
  const mockApi = require('./devUtils/mockApi').default;
  mockApi();
}

const store = configureStore(preloadedState);

render(
  <Provider store={store}>
    <Root />
  </Provider>,
  document.getElementById('root')
);
registerServiceWorker();