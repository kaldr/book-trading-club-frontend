import signIn, {initState} from './signIn';
import * as actions from '../actions/signin-up-out/signInActions';

describe('signIn reducer', () => {
  test('should return the initial state', () => {
    expect(signIn(undefined, {})).toEqual(initState);
  });

  test('sign in set field', () => {
    const field = {username: 'username'};
    expect(signIn(initState, actions.signInSetField(field))).toEqual({
      ...initState,
      ...field,
    });
  });

  test('change sign in button state', () => {
    const signInButtonStatus = 'done';
    expect(signIn(initState, actions.changeSignInButtonStatus(signInButtonStatus))).toEqual({
      ...initState,
      signInButtonStatus,
    });
  });

  test('set sign in error', () => {
    const error = 'error';
    expect(signIn(initState, actions.setSignInError(error))).toEqual({
      ...initState,
      error,
    });
  });
});