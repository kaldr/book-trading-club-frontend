import {
  SET_TRADE_REQUEST_STATUS,
  SET_TRADE_REQUEST_FAILURE,
} from '../actions/actionTypes';

export const initState = {
  error: '',
  tradeStatus: {},
};

const trades = (state = initState, action) => {
  switch (action.type) {
  case SET_TRADE_REQUEST_STATUS:
    const {status, bookId} = action;
    return {...state, tradeStatus: {
      ...state.tradeStatus,
      [bookId]: status,
    }};
  case SET_TRADE_REQUEST_FAILURE:
    return {...state, error: action.error};
  default:
    return state;
  }
};

export default trades;