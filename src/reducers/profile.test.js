import profile, {initState} from './profile';
import * as actions from '../actions/profile/profileActions';
import {addNewTrade} from '../actions/books/booksActions';

describe('profile reducer', () => {
  test('should return the initial state', () => {
    expect(profile(undefined, {})).toEqual(initState);
  });

  test('set profile save status', () => {
    const saveStatus = 'inProcess';
    expect(profile(initState, actions.setProfileSaveStatus(saveStatus))).toEqual({
      ...initState,
      saveStatus,
    });
  });

  test('set profile save failure', () => {
    const error = 'error';
    const saveStatus = 'waitForClick';
    expect(profile(initState, actions.setProfileSaveFailure(error))).toEqual({
      ...initState,
      error,
      saveStatus,
    });
  });

  test('set editing status', () => {
    const editing = true;
    expect(profile(initState, actions.setEditingStatus(editing))).toEqual({
      ...initState,
      editing,
    });
  });

  test('set user info', () => {
    const userInfo = {
      fullname: 'fullname',
      city: 'city',
      //stateName: '',
    };

    expect(profile(initState, actions.setUserInfo(userInfo))).toEqual({
      ...initState,
      userInfo: {
        ...initState.userInfo,
        ...userInfo,
      },
    });
  });

  test('set temp user info', () => {
    const tempUserInfo = {
      fullname: 'fullname',
      //city: 'city',
      stateName: '',
    };

    expect(profile(initState, actions.setTempUserInfo(tempUserInfo))).toEqual({
      ...initState,
      tempUserInfo: {
        ...initState.tempUserInfo,
        ...tempUserInfo,
      },
    });
  });

  test('set trade button status', () => {
    const tradeId = 1;
    const status = 'inProcess';
    const state = {
      ...initState,
      tradesForUser: [
        {tradeId: 1, tradeButtonsStatus: 'tradeButtonsStatus'},
        {tradeId: 2, tradeButtonsStatus: 'tradeButtonsStatus'},
      ],
    };
    expect(profile(state, actions.setTradeButtonsStatus(tradeId, status))).toEqual({
      ...state,
      tradesForUser: [
        {tradeId: 1, tradeButtonsStatus: status},
        {tradeId: 2, tradeButtonsStatus: 'tradeButtonsStatus'},
      ],
    });
  });

  test('set trade accept', () => {
    const tradeId = 1;
    const accepted = true;
    const state = {
      ...initState,
      tradesForUser: [
        {tradeId: 1, accepted: false},
        {tradeId: 2, accepted: false},
      ],
    };
    expect(profile(state, actions.setTradeAccept(tradeId, accepted))).toEqual({
      ...state,
      tradesForUser: [
        {tradeId: 1, accepted: true},
        {tradeId: 2, accepted: false},
      ],
    });
  });

  test('set trade cancel', () => {
    const tradeId = 1;
    const state = {
      ...initState,
      tradesForUser: [
        {tradeId: 1},
        {tradeId: 2},
      ],
    };
    expect(profile(state, actions.setTradeCancel(tradeId))).toEqual({
      ...state,
      tradesForUser: [
        {tradeId: 2},
      ],
    });
  });

  test('add new trade', () => {
    const book = {id: '2'};
    const state = {
      ...initState,
      userTrades: [{id: '1'}],
    };
    expect(profile(state, addNewTrade(book))).toEqual({
      ...state,
      userTrades: [
        {id: '1'},
        {id: '2'},
      ],
    });
  });

  test('set profile initial state error', () => {
    const error = 'error';
    expect(profile(initState, actions.setProfileInitialStateError(error))).toEqual({
      ...initState,
      initialState: {
        ...initState.initialState,
        error,
      },
    });
  });

  test('set profile initial state loading', () => {
    const loading = false;
    expect(profile(initState, actions.setProfileInitialStateLoading(loading))).toEqual({
      ...initState,
      initialState: {
        ...initState.initialState,
        loading,
      },
    });
  });

  test('set profile trades', () => {
    const userTrades = [{id: '2'}];
    const tradesForUser = [{tradeId: '2'}];
    
    const state = {
      ...initState,
      userTrades: [{id: '1'}],
      tradesForUser: [{tradeId: '1'}],
    };

    expect(profile(state, actions.setProfileTrades({userTrades, tradesForUser}))).toEqual({
      ...state,
      userTrades: [
        {id: '2'},
      ],
      tradesForUser: [
        {tradeId: '2'},
      ],
    });
  });
});