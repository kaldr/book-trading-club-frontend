import trades, {initState} from './trades';
import * as actions from '../actions/books/booksActions';

describe('trades reducer', () => {
  test('should return the initial state', () => {
    expect(trades(undefined, {})).toEqual(initState);
  });

  test('set trade request status', () => {
    const bookId = 1;
    const status = 'inProcess';
    expect(trades(initState, actions.setTradeRequestStatus(status, bookId))).toEqual({
      ...initState,
      tradeStatus: {
        ...initState.tradeStatus,
        [bookId]: status,
      },
    });
  });

  test('set trade request failure', () => {
    const error = 'error';
    expect(trades(initState, actions.setTradeRequestFailure(error))).toEqual({
      ...initState,
      error,
    });
  });
});