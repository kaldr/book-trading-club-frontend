import {
  RECIEVE_SEARCHED_BOOKS,
  SEARCHED_BOOKS_FAILURE,
  SET_TEXT_TO_SEARCH,
  SET_IS_SEARCHING,
  CHANGE_ADD_BUTTON_STATE,
} from '../actions/actionTypes';

export const initState = {
  textToSearch: '',
  books: [],
  isSearching: false,
  error: '',
};

const addBookState = (state = initState, action) => {
  switch (action.type) {
  case RECIEVE_SEARCHED_BOOKS:
    return {
      ...state,
      books: action.books,
      isSearching: false,
    };
  case SEARCHED_BOOKS_FAILURE:
    return {
      ...state,
      error: action.error,
      isSearching: false,
    };
  case SET_TEXT_TO_SEARCH:
    return {
      ...state,
      textToSearch: action.textToSearch,
    };
  case SET_IS_SEARCHING:
    return {
      ...state,
      isSearching: action.isSearching,
    };
  case CHANGE_ADD_BUTTON_STATE:
    const {books} = state;
    return {
      ...state,
      books: books.map(book => {
        if (action.id !== book.id) {
          return book;
        }
        return {
          ...book,
          addButtonState: action.addButtonState,
        };
      }),
    };
  default:
    return state;
  }
};

export default addBookState;