import {SET_USERNAME} from '../actions/actionTypes';

export const initState = '';

const username = (state = initState, action) => {
  if (action.type === SET_USERNAME) {
    return action.username;
  }
  return state;
};

export default username;