import {
  SET_PROFILE_SAVE_STATUS,
  SET_PROFILE_SAVE_FAILURE,
  SET_EDITING_STATUS,
  SET_TEMP_USER_INFO,
  SET_USER_INFO,
  SET_TRADE_BUTTONS_STATUS,
  SET_TRADE_ACCEPT,
  SET_TRADE_CANCEL,
  SET_PROFILE_INITIAL_STATE_ERROR,
  SET_PROFILE_INITIAL_STATE_LOADING,
  SET_PROFILE_TRADES,
  ADD_NEW_TRADE,
} from '../actions/actionTypes';

export const initState = {
  userInfo: {
    fullname: '',
    city: '',
    stateName: '',
  },
  tempUserInfo: {
    fullname: '',
    city: '',
    stateName: '',
  },
  userTrades: [],
  tradesForUser: [],
  editing: false,
  error: '',
  saveStatus: 'waitForClick',
  initialState: {
    error: '',
    loading: true,
  },
};

const profile = (state = initState, action) => {
  switch (action.type) {
  case SET_PROFILE_SAVE_STATUS:
    const {saveStatus} = action;
    return {...state, saveStatus};
  case SET_PROFILE_SAVE_FAILURE:
    const {error} = action;
    return {...state, saveStatus: 'waitForClick', error};
  case SET_EDITING_STATUS:
    const {editing} = action;
    return {...state, editing};
  case SET_USER_INFO:
    return {
      ...state,
      userInfo: {
        ...state.userInfo,
        ...action.info,
      }
    };
  case SET_TEMP_USER_INFO:
    return {
      ...state,
      tempUserInfo: {
        ...state.tempUserInfo,
        ...action.info,
      }
    };
  case SET_TRADE_BUTTONS_STATUS:
    return {
      ...state,
      tradesForUser: state.tradesForUser.map(trade => {
        if (trade.tradeId !== action.tradeId) {
          return trade;
        }
        return {...trade, tradeButtonsStatus: action.status};
      }),
    };
  case SET_TRADE_ACCEPT:
    return {
      ...state,
      tradesForUser: state.tradesForUser.map(trade => {
        if (trade.tradeId !== action.tradeId) {
          return trade;
        }
        return {...trade, accepted: true};
      }),
    };
  case SET_TRADE_CANCEL:
    return {
      ...state,
      tradesForUser: state.tradesForUser.filter(trade =>
        trade.tradeId !== action.tradeId
      ),
    };
  case ADD_NEW_TRADE:
    return {
      ...state,
      userTrades: [...state.userTrades, action.book],
    };
  case SET_PROFILE_INITIAL_STATE_ERROR:
    return {
      ...state,
      initialState: {
        ...state.initialState,
        error: action.error,
      },
    };
  case SET_PROFILE_INITIAL_STATE_LOADING:
    return {
      ...state,
      initialState: {
        ...state.initialState,
        loading: action.loading,
      },
    };
  case SET_PROFILE_TRADES:
    const {userTrades, tradesForUser} = action.trades;
    return {
      ...state,
      userTrades,
      tradesForUser,
    };
  default:
    return state;
  }
};

export default profile;