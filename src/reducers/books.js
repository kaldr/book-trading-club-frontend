import {
  RECIEVE_BOOKS,
  ADD_BOOK,
} from '../actions/actionTypes';

export const initState = [];

const books = (state = initState, action) => {
  switch (action.type) {
  case RECIEVE_BOOKS:
    return action.books;
  case ADD_BOOK:
    return [action.book, ...state];
  default:
    return state;
  }
};

export default books;