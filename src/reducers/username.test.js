import username, {initState} from './username';

describe('username reducer', () => {
  test('should return the initial state', () => {
    expect(username(undefined, {})).toEqual(initState);
  });
});