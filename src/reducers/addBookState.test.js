import * as actions from '../actions/add-book/addBookActions';
import addBookState, {initState} from './addBookState';

describe('add book reducer', () => {
  test('should return the initial state', () => {
    expect(addBookState(undefined, {})).toEqual(initState);
  });

  test('recieve searched books', () => {
    const books = ['books'];
    expect(
      addBookState(initState, actions.recieveSearchedBooks(books))
    ).toEqual({
      ...initState,
      books,
      isSearching: false,
    });
  });

  test('searched books failure', () => {
    const error = {message: 'error'};
    expect(
      addBookState(initState, actions.searchedBooksFailure(error))
    ).toEqual({
      ...initState,
      error,
      isSearching: false,
    });
  });

  test('set text to search', () => {
    const textToSearch = 'textToSearch';
    expect(
      addBookState(initState, actions.setTextToSearch(textToSearch))
    ).toEqual({
      ...initState,
      textToSearch,
    });
  });

  test('set isSearching', () => {
    const isSearching = true;
    expect(
      addBookState(initState, actions.setIsSearching(isSearching))
    ).toEqual({
      ...initState,
      isSearching,
    });
  });

  test('change addButton state', () => {
    const addButtonState = 'done';
    const id = '1';
    expect(
      addBookState({
        ...initState,
        books: [
          {id: '1', addButtonState: 'waitForClick'},
          {id: '2', addButtonState: 'waitForClick'},
        ],
      }, actions.changeAddButtonState(addButtonState, id))
    ).toEqual({
      ...initState,
      books: [
        {id: '1', addButtonState: 'done'},
        {id: '2', addButtonState: 'waitForClick'},
      ],
    });
  });
});