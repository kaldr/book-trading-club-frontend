import {combineReducers} from 'redux';

import username from './username';
import books from './books';
import addBookState from './addBookState';
import trades from './trades';
import profile from './profile';
import initialState from './initialStateReducer';
import signIn from './signIn';

const rootReducer = combineReducers({
  username,
  books,
  addBookState,
  trades,
  profile,
  initialState,
  signIn,
});

export default rootReducer;