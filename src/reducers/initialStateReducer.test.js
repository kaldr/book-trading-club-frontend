import initialState, {initState} from './initialStateReducer';
import * as actions from '../actions/initial-state/initialState';

describe('initial state reducer', () => {
  test('set initial state error', () => {
    const error = 'error';
    expect(initialState(initState, actions.setInitialStateError(error))).toEqual({
      ...initState,
        error,
    });
  });

  test('set initial state loading', () => {
    const loading = false;
    expect(initialState(initState, actions.setInitialStateLoading(loading))).toEqual({
      ...initState,
        loading,
    });
  });
});