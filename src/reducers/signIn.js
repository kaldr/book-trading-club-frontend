import {
  SIGN_IN_SET_FIELD,
  CHANGE_SIGN_IN_BUTTON_STATUS,
  SET_SIGN_IN_ERROR,
} from '../actions/actionTypes';

export const initState = {
  username: '',
  password: '',
  signInButtonStatus: 'waitForClick',
  error: '',
};

const signIn = (state = initState, action) => {
  switch (action.type) {
  case SIGN_IN_SET_FIELD:
    return {
      ...state,
      ...action.field,
    };
  case CHANGE_SIGN_IN_BUTTON_STATUS:
    const {signInButtonStatus} = action;
    return {
      ...state,
      signInButtonStatus,
    };
  case SET_SIGN_IN_ERROR:
    const {error} = action;
    return {
      ...state,
      error,
    };
  default:
    return state;
  }
};

export default signIn;