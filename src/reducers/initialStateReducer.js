import {
  SET_INITIAL_STATE_ERROR,
  SET_INITIAL_STATE_LOADING,
} from '../actions/actionTypes';

export const initState = {
  error: '',
  loading: true,
};

const initialState = (state = initState, action) => {
  if (action.type === SET_INITIAL_STATE_ERROR) {
    return {...state, error: action.error};
  } else if (action.type === SET_INITIAL_STATE_LOADING) {
    return {...state, loading: action.loading};
  }
  return state;
};

export default initialState;