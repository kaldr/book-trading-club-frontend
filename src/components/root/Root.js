import React, {useEffect} from 'react';
import {connect, useSelector} from 'react-redux';
import {Route, Switch, BrowserRouter as Router} from 'react-router-dom';
import styled, {createGlobalStyle} from 'styled-components';

import Books from '../books/Books';
import Header from '../header/Header';
import AddBook from '../add-book/AddBook';
import Profile from '../profile/Profile';
import {UnAuthenticated} from '../signin-up-out/SignInUp';
import {initialStateHandler} from '../../actions/initial-state/initialState';

const GlobalStyle = createGlobalStyle`
  :root {
    box-sizing: border-box;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Ubuntu, 'Helvetica Neue', sans-serif;
  }
  *, *::before, *::after {
    box-sizing: inherit;
  }
  body {
    margin: 0;
    color: #151d3f;
    background-color: #fefefe;
  }
`;

const Wrapper = styled.div`
  
`;

function App() {
  const username = useSelector(state => state.username);
  return (
    <Wrapper>
      <Header />
      {username ? <AddBook /> : <UnAuthenticated />}
      <Books />
    </Wrapper>
  );
}

function ProfilePage() {
  const username = useSelector(state => state.username);
  return (
    <Wrapper>
      <Header />
      {username && <Profile />}
    </Wrapper>
  );
}

function Root({error, loading, initialStateHandler}) {
  useEffect(() => {
    initialStateHandler();
  }, [initialStateHandler]);

  if (error) {
    return <div>{error}</div>;
  }

  if (loading) {
    return <div>Loading...</div>
  }

  return (
    <div>
      <GlobalStyle />
      <Router>
        <Switch>
          <Route exact path="/" component={App} />
          <Route exact path="/profile" component={ProfilePage} />
        </Switch>
      </Router>
    </div>
  );
};

const RootContainer = connect(
  ({initialState}) => ({...initialState}),
  {initialStateHandler}
)(Root);

export default RootContainer;