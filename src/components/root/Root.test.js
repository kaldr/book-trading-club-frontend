import React from 'react';
import {createStore, applyMiddleware} from 'redux';
import {render} from '@testing-library/react';
import thunk from 'redux-thunk';
import {Provider} from 'react-redux';
import MockAdapter from 'axios-mock-adapter';

import reducer from '../../reducers/rootReducer';
import Root from './Root';
import axios from '../../axios';

const mockApi = new MockAdapter(axios);

function renderWithRedux(ui, initialState) {
  const store = createStore(reducer, initialState, applyMiddleware(thunk));
  return {
    ...render(<Provider store={store}>{ui}</Provider>),
    store,
  };
}

test('there is error', async () => {
  const error = 'Request failed with status code 403';
  mockApi.onGet('/api/initstate').reply(403);
  const {getByText, findByText, queryByText} = renderWithRedux(<Root />);
  expect(getByText('Loading...')).toBeDefined();
  expect(queryByText(error)).toBe(null);
  expect(await findByText(error)).toBeDefined();
  expect(queryByText('Loading...')).toBe(null);
});

test('there is no error, authenticated user', async () => {
  const username = 'username';
  const books = [{id: '1', userBookId: '1'}, {id: '2', userBookId: '2'}];
  mockApi.onGet('/api/initstate').reply(200, {
    username,
    books,
  });
  const {getByText, getByPlaceholderText, getAllByText, findByText, queryByText} = renderWithRedux(<Root />);
  expect(getByText('Loading...')).toBeDefined();
  expect(await findByText('Book Trading Club')).toBeDefined();
  expect(getByPlaceholderText('Search and Add Books...')).toBeDefined();
  expect(getByText('Profile')).toBeDefined();
  expect(getAllByText('Trade')).toBeDefined();
  expect(queryByText('Loading...')).toBe(null);
});

test('there is no error, anonymous user', async () => {
  const username = '';
  const books = [{id: '1', userBookId: '1'}, {id: '2', userBookId: '2'}];
  mockApi.onGet('/api/initstate').reply(200, {
    username,
    books,
  });
  const {getByText, queryByPlaceholderText, getAllByText, findByText, queryByText} = renderWithRedux(<Root />);
  expect(getByText('Loading...')).toBeDefined();
  expect(await findByText('Book Trading Club')).toBeDefined();
  expect(queryByText('Profile')).toBe(null);
  expect(queryByPlaceholderText('Search and Add Books...')).toBe(null);
  const trades = getAllByText('Trade');
  expect(trades).toBeDefined();
  let allTradeButtonsAreDisabled = true;
  for (const trade of trades) {
    if (!trade.disabled) {
      allTradeButtonsAreDisabled = false;
      return;
    }
  }
  expect(allTradeButtonsAreDisabled).toBe(true);
  expect(queryByText('Loading...')).toBe(null);
});