import React, {useState} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import styled from 'styled-components';

import axios from '../../axios';
import {
  signInHandler,
  signInSetField,
} from '../../actions/signin-up-out/signInActions';
import {setUsername} from '../../actions/username/usernameActions';
import {
  ButtonsWrapper,
  Input,
  InputWrapper,
} from '../profile/Profile';
import {Button} from '../books/Books';

const Form = styled.form`
  border: 1px solid black;
  border-top: none;
  padding: .5rem;
  min-height: 14rem;
  max-width: 14.25rem;
`;

const Error = styled.div`
  font-size: .8rem;
  padding-top: .5rem;
  color: red;
`;

function SignUp({setUsername}) {
  const [fields, setFields] = useState({
    username: '',
    password: '',
    confirmPassword: '',
  });
  const [error, setError] = useState('');
  const [buttonStatus, setButtonStatus] = useState('waitForClick');

  function setField(field, value) {
    setFields(fields => ({
      ...fields,
      [field]: value,
    }));
  }
  
  async function signUpHandler() {
    setButtonStatus('inProcess');
    if (fields.password !== fields.confirmPassword) {
      setError("Passwords don't match");
      setButtonStatus('waitForClick');
      return;
    }
    try {
      const {username, password} = fields;
      const res = await axios.post('/api/signup', {username, password});
      setButtonStatus('done');
      setUsername(res.data.username);
    } catch (err) {
      const errorMessage = err.response ? (err.response.data ? err.response.data.message : err.message) : err.message;
      setError(errorMessage);
      setButtonStatus('waitForClick');
    }
  }

  return (
    <Form 
      onSubmit={event => {
        event.preventDefault();
        signUpHandler();
      }}
    >
      <InputWrapper>
        <Input
          onChange={({target: {value}}) => setField('username', value.trim())}
          type="text" placeholder="Username" autoFocus
        />
      </InputWrapper>

      <InputWrapper>
        <Input
          onChange={({target: {value}}) => setField('password', value)}
          type="password" placeholder="Password"
        />
      </InputWrapper>

      <InputWrapper>
        <Input
          onChange={({target: {value}}) => setField('confirmPassword', value)}
          type="password" placeholder="Confirm Password"
        />
      </InputWrapper>

      <Button
        disabled={
          buttonStatus !== 'waitForClick' || fields.username.length === 0 ||
          fields.password.length === 0 || fields.confirmPassword.length === 0
        }
      >Sign Up</Button>

      {error && <Error>{error}</Error>}
    </Form>
  );
}

const SignUpContainer = withRouter(connect(undefined, {setUsername})(SignUp));

export const SignIn = ({signInSetField, signInHandler, error, signInButtonStatus}) => (
  <Form 
    onSubmit={event => {
      event.preventDefault();
      signInHandler();
    }}
  >
    <InputWrapper>
      <Input
        onChange={e => signInSetField({username: e.target.value.trim()})}
        type="text" name="username" placeholder="Username" autoFocus
      />
    </InputWrapper>
    
    <InputWrapper>
      <Input
        onChange={e => signInSetField({password: e.target.value})}
        type="password" placeholder="Password"
      />
    </InputWrapper>

    <Button
      disabled={signInButtonStatus !== 'waitForClick'}
    >Sign In</Button>

    {error && <Error>{error}</Error>}
  </Form>
);

export const SignInContainer = withRouter(connect(
  ({signIn}) => ({
    error: signIn.error,
    signInButtonStatus: signIn.signInButtonStatus,
  }),
  {signInHandler, signInSetField}
)(SignIn));

const SignInSignUpFormWrapper = styled.div`
  padding: .5rem;
  display: inline-flex;
  flex-flow: column;
`;

const SignInUpButton = styled.button`
  background-color: white;
  color: black;
  border: 1px solid black;
  ${props => props.active && `
    border-bottom: none;
    border-top: 3px solid black;
  `}
  padding: .5rem;
  cursor: pointer;
`;

function SignInSignUpForm({signIn = false}) {
  const [isSignIn, setIsSignIn] = useState(signIn);

  return (
    <SignInSignUpFormWrapper>
      <ButtonsWrapper>
        <SignInUpButton
          active={isSignIn === true}
          onClick={() => setIsSignIn(true)}
        >Sign In</SignInUpButton>
        <SignInUpButton
          active={isSignIn === false}
          onClick={() => setIsSignIn(false)}
        >Sign Up</SignInUpButton>
      </ButtonsWrapper>
      {isSignIn ? <SignInContainer /> : <SignUpContainer />}
    </SignInSignUpFormWrapper>
  );
}

const UnAuthenticatedWrapper = styled.div`
  max-width: 1100px;
  margin: 0 auto;
`;

export function UnAuthenticated() {
  return (
    <UnAuthenticatedWrapper>
      <SignInSignUpForm signIn />
    </UnAuthenticatedWrapper>
  );
}

export default SignInSignUpForm;