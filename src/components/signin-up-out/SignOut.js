import React, {useState} from 'react';
import {useDispatch} from 'react-redux';
import styled from 'styled-components';

import axios from '../../axios';
import {setUsername} from '../../actions/username/usernameActions';

const Button = styled.button`
  color: #151d3f;
  border: 1px solid #b7bac4;
  padding: .2rem .8rem;
  font-size: 1rem;
  &:hover {
    background-color: rgb(240, 240, 240);
  }
  cursor: pointer;
`;

function SignOut() {
  const [disabled, setDisabled] = useState(false);
  const dispatch = useDispatch();
  
  async function signOutHandler() {
    setDisabled(true);
    try {
      await axios.post('/api/signout');
      localStorage.setItem('authHeader', '');
      dispatch(setUsername(''));
    } catch (err) {
      setDisabled(false);
    }
  }

  return (
    <Button
      onClick={signOutHandler}
      disabled={disabled}
    >Sign out</Button>
  );
}

export default SignOut;