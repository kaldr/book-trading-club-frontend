import React from 'react';
import Enzyme, {shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import * as components from './SignInUp';

Enzyme.configure({adapter: new Adapter()});

describe('SignIn', () => {
  function setup(props) {
    return shallow(<components.SignIn {...props} />);
  }

  test('there is error', () => {
    const props = {
      error: 'error',
    };
    const SignIn = setup(props);
    const error = SignIn.find('.error');
    expect(error.length).toBe(1);
    expect(error.text()).toBe(props.error);
  });

  test('there is no error', () => {
    const props = {
      error: '',
    };
    const SignIn = setup(props);
    const error = SignIn.find('.error');
    expect(error.length).toBe(0);
  });
});