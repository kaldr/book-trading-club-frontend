import React from 'react';
import Enzyme, {shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import * as components from './Profile';
import {Button} from '../books/Books';
import {Thumbnail} from '../add-book/AddBook';

Enzyme.configure({adapter: new Adapter()});

describe('Editing', () => {
  function setup(props) {
    return shallow(<components.Editing {...props} />);
  }

  test('inputs', () => {
    const props = {
      tempUserInfo: {
        fullname: 'fullname',
        city: 'city',
        stateName: 'stateName',
      },
      setTempUserInfo: jest.fn(),
    };
    const Editing = setup(props);
    
    const inputs = Editing.find(components.Input);
    expect(inputs.length).toBe(3);
    
    expect(inputs.get(0).props.value).toBe(props.tempUserInfo.fullname);
    expect(inputs.get(1).props.value).toBe(props.tempUserInfo.city);
    expect(inputs.get(2).props.value).toBe(props.tempUserInfo.stateName);

    const newFullname = 'newFullname';
    inputs.at(0).simulate('change', {target: {value: newFullname}});
    expect(props.setTempUserInfo.mock.calls.length).toBe(1);
    expect(props.setTempUserInfo.mock.calls[0][0]).toEqual({fullname: newFullname});

    props.setTempUserInfo.mockClear();
    const newCity = 'newCity';
    inputs.at(1).simulate('change', {target: {value: newCity}});
    expect(props.setTempUserInfo.mock.calls.length).toBe(1);
    expect(props.setTempUserInfo.mock.calls[0][0]).toEqual({city: newCity});

    props.setTempUserInfo.mockClear();
    const newStateName = 'newStateName';
    inputs.at(2).simulate('change', {target: {value: newStateName}});
    expect(props.setTempUserInfo.mock.calls.length).toBe(1);
    expect(props.setTempUserInfo.mock.calls[0][0]).toEqual({stateName: newStateName});
  });

  test('buttons', () => {
    const props = {
      tempUserInfo: {
        fullname: 'fullname',
        city: 'city',
        stateName: 'stateName',
      },
      saveHandler: jest.fn(),
      cancelEdit: jest.fn(),
    };
    
    const Editing = setup(props);
    const buttons = Editing.find(Button);
    
    expect(buttons.length).toBe(2);
    
    expect(buttons.at(0).text()).toBe('Save');
    expect(buttons.at(1).text()).toBe('Cancel');

    /*buttons.at(0).simulate('click');
    expect(props.saveHandler.mock.calls.length).toBe(1);
    expect(props.saveHandler.mock.calls[0][0]).toEqual(props.tempUserInfo);*/

    buttons.at(1).simulate('click');
    expect(props.cancelEdit.mock.calls.length).toBe(1);
  });

  test('buttons are disable', () => {
    const props = {
      tempUserInfo: {},
      saveStatus: 'inProcess',
    };

    const Editing = setup(props);
    const buttons = Editing.find(Button);

    expect(buttons.at(0).prop('disabled')).toBe(true);
    expect(buttons.at(1).prop('disabled')).toBe(true);
  });

  test('buttons are not disable', () => {
    const props = {
      tempUserInfo: {},
      saveStatus: 'not in process',
    };

    const Editing = setup(props);
    const buttons = Editing.find(Button);

    expect(buttons.at(0).prop('disabled')).toBe(false);
    expect(buttons.at(1).prop('disabled')).toBe(false);
  });
});

describe('UserInfo', () => {
  function setup(props) {
    return shallow(<components.UserInfo {...props} />);
  }

  test('editing status', () => {
    let props = {editing: false};
    let UserInfo = setup(props);
    expect(UserInfo.find(components.EditingContainer).length).toBe(0);
    expect(UserInfo.find(Button).length).toBe(1);

    props = {editing: true};
    UserInfo = setup(props);
    expect(UserInfo.find(components.EditingContainer).length).toBe(1);
    expect(UserInfo.find(Button).length).toBe(0);
  });

  test('UserInfo edit button', () => {
    const props = {setEditingStatus: jest.fn()};
    const UserInfo = setup(props);
    const editButton = UserInfo.find(Button);
    expect(editButton.text()).toBe('Edit');
    editButton.simulate('click');
    expect(props.setEditingStatus.mock.calls.length).toBe(1);
    expect(props.setEditingStatus.mock.calls[0][0]).toBe(true);
  });
});

describe('UserTrade', () => {
  function setup(props) {
    return shallow(<components.UserTrade {...props} />);
  }

  test('UserTrade', () => {
    let props = {book: {}};
    let UserTrade = setup(props);
    expect(UserTrade.find(Thumbnail).length).toBe(1);
    expect(UserTrade.find(Button).text()).toBe('Not Accepted');

    props = {book: {accepted: true}};
    UserTrade = setup(props);
    expect(UserTrade.find(Button).text()).toBe('Accepted');
  });
});

describe('UserTrades', () => {
  test('UserTrades', () => {
    const books = [{id: 0}, {id: 1}];
    const UserTrades = shallow(<components.UserTrades books={books} />);

    expect(UserTrades.find(components.Title).length).toBe(1);
    expect(UserTrades.find(components.UserTrade).length).toBe(2);
  });
});

describe('TradeForUser', () => {
  function setup(props) {
    return shallow(<components.TradeForUser {...props} />);
  }

  test('accepted trade', () => {
    const props = {
      book: {accepted: true},
    };
    const TradeForUser = setup(props);
    expect(TradeForUser.find(Thumbnail).length).toBe(1);
    expect(TradeForUser.find(Button).text()).toBe('Accepted');
  });

  test('not accepted trade', () => {
    const props = {
      book: {
        tradeId: 0,
        accepted: false,
      },
    };
    const TradeForUser = setup(props);
    expect(TradeForUser.find(Thumbnail).length).toBe(1);
    expect(TradeForUser.find(Button).length).toBe(2);
  });

  test('TradeForUser buttons not disabled', () => {
    const props = {
      book: {
        tradeId: 0,
        accepted: false,
        tradeButtonsStatus: 'not inProcess',
      },
      acceptTradeRequset: jest.fn(),
      cancelTradeRequest: jest.fn(),
    };
    const TradeForUser = setup(props);
    const buttons = TradeForUser.find(Button);
    expect(buttons.at(0).prop('disabled')).toBe(false);
    expect(buttons.at(1).prop('disabled')).toBe(false);

    expect(buttons.at(0).text()).toBe('Accept');
    expect(buttons.at(1).text()).toBe('Cancel');

    buttons.at(0).simulate('click');
    expect(props.acceptTradeRequset.mock.calls.length).toBe(1);
    expect(props.acceptTradeRequset.mock.calls[0][0]).toBe(props.book.tradeId);

    buttons.at(1).simulate('click');
    expect(props.cancelTradeRequest.mock.calls.length).toBe(1);
    expect(props.cancelTradeRequest.mock.calls[0][0]).toBe(props.book.tradeId);
  });

  test('TradeForUser buttons disabled', () => {
    const props = {
      book: {
        tradeId: 0,
        accepted: false,
        tradeButtonsStatus: 'inProcess',
      },
    };
    const TradeForUser = setup(props);
    const buttons = TradeForUser.find(Button);
    expect(buttons.at(0).prop('disabled')).toBe(true);
    expect(buttons.at(1).prop('disabled')).toBe(true);
  });
});

describe('TradesForUser', () => {
  test('TradesForUser', () => {
    const books = [{id: 0}, {id: 1}];
    const UserTrades = shallow(<components.TradesForUser books={books} />);

    expect(UserTrades.find(components.Title).length).toBe(1);
    expect(UserTrades.find(components.TradeForUser).length).toBe(2);
  });
});

describe('Profile', () => {
  function setup(props) {
    return shallow(<components.Profile {...props} />);
  }

  test('call profile initial state handler on mount when not loaded yet', () => {
    const props = {
      loading: true, // not loaded yet
      profileInitialStateHandler: jest.fn(),
    };
    setup(props);
    expect(props.profileInitialStateHandler.mock.calls.length).toBe(1);
  });

  test("call profile initial state handler on every mount", () => {
    const props = {
      loading: false, // loaded
      profileInitialStateHandler: jest.fn(),
    };
    setup(props);
    expect(props.profileInitialStateHandler.mock.calls.length).toBe(1);
  });

  test('there is error', () => {
    const props = {
      error: 'error',
      profileInitialStateHandler: jest.fn(),
    };
    const Profile = setup(props);
    const div = Profile.find(components.ProfileWrapper);
    expect(div.length).toBe(1);
    expect(div.text()).toBe(props.error);
  });

  test('loading', () => {
    const props = {
      loading: true,
      error: '',
      profileInitialStateHandler: jest.fn(),
    };
    const Profile = setup(props);
    const div = Profile.find(components.ProfileWrapper);
    expect(div.length).toBe(1);
    expect(div.text()).toBe('Loading...');
  });

  test('Profile', () => {
    const props = {
      loading: false,
      error: '',
      profileInitialStateHandler: jest.fn(),
    };
    const Profile = setup(props);
    expect(Profile.find(components.UserInfoContainer).length).toBe(1);
    expect(Profile.find(components.UserTradesContainer).length).toBe(1);
    expect(Profile.find(components.TradesForUserContainer).length).toBe(1);
  });
});