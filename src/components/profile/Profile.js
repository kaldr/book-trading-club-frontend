import React from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import styled from 'styled-components';

import {Thumbnail} from '../add-book/AddBook';
import {
  BooksWrapper,
  BookWrapper,
  Button,
} from '../books/Books';
import {
  saveHandler,
  setEditingStatus,
  setTempUserInfo,
  cancelEdit,
  acceptTradeRequset,
  cancelTradeRequest,
  profileInitialStateHandler,
} from '../../actions/profile/profileActions';

const EditingWrapper = styled.form`

`;

const UserInfoButtonsWrapper = styled.div`
  display: flex;
  & > button:first-child {
    border-right: none;
  }
`;

export const Input = styled.input`
  border: none;
  background-color: #eee;
  padding: .5rem;
`;

export const InputWrapper = styled.div`
  padding-bottom: .5rem;
  &:last-child {
    padding-bottom: 0;
  }
`;

export const Editing = ({saveStatus, tempUserInfo, setTempUserInfo, saveHandler, cancelEdit}) => (
  <EditingWrapper
    onSubmit={event => {
      event.preventDefault();
      saveHandler(tempUserInfo);
    }}
  >
    <InputWrapper>
      <Input
        value={tempUserInfo.fullname}
        onChange={event => setTempUserInfo({fullname: event.target.value})}
        placeholder="Name"
        autoFocus
      />
    </InputWrapper>
    <InputWrapper>
      <Input
        value={tempUserInfo.city}
        onChange={event => setTempUserInfo({city: event.target.value})}
        placeholder="City"
      />
    </InputWrapper>
    <InputWrapper>
      <Input
        value={tempUserInfo.stateName}
        onChange={event => setTempUserInfo({stateName: event.target.value})}
        placeholder="State"
      />
    </InputWrapper>
    <UserInfoButtonsWrapper>
      <Button
        disabled={saveStatus === 'inProcess'}
        type="submit"
      >Save</Button>
      <Button
        disabled={saveStatus === 'inProcess'}
        onClick={cancelEdit}
      >Cancel</Button>
    </UserInfoButtonsWrapper>
  </EditingWrapper>
);

export const EditingContainer = withRouter(connect(
  ({profile}) => ({
    saveStatus: profile.saveStatus,
    tempUserInfo: profile.tempUserInfo,
  }),
  {
    saveHandler,
    setTempUserInfo,
    cancelEdit,
  }
)(Editing));

const UserInfoWrapper = styled.div`
  & > button {
    margin-top: .8rem;
  }
`;

const InfoFieldTitle = styled.span`
  font-size: .8rem;
`;

const InfoFieldValue = styled.span`
  padding-left: .5rem;
`;

const InfoField = ({title, value}) => (
  <div>
    <InfoFieldTitle>{title}</InfoFieldTitle>
    <InfoFieldValue>{value}</InfoFieldValue>
  </div>
);

export const UserInfo = ({editing, fullname, city, stateName, setEditingStatus}) => {
  if (editing) {
    return <EditingContainer />;
  }
  return (
    <UserInfoWrapper>
      <InfoField title="Name:" value={fullname} />
      <InfoField title="City:" value={city} />
      <InfoField title="State:" value={stateName} />
      <Button 
        onClick={() => setEditingStatus(true)}
      >Edit</Button>
    </UserInfoWrapper>
  );
};

export const UserInfoContainer = withRouter(connect(
  ({profile}) => ({
    ...profile.userInfo,
    editing: profile.editing,
  }),
  {setEditingStatus}
)(UserInfo));

export const UserTrade = ({book}) => (
  <BookWrapper>
    <Thumbnail book={book} />
    <Button disabled>{book.accepted ? 'Accepted' : 'Not Accepted'}</Button>
  </BookWrapper>
);

export const Title = styled.h3`
  font-weight: normal;
`;

export const UserTrades = ({books}) => (
  <>
    <Title>Others' books that you want:</Title>
    <BooksWrapper>
      {books.map(book => (
        <UserTrade key={book.id} book={book}/>
      ))}
    </BooksWrapper>
  </>
);

export const UserTradesContainer = withRouter(connect(
  ({profile}) => ({books: profile.userTrades})
)(UserTrades));

export const ButtonsWrapper = styled.div`
  display: flex;
  & > button {
    width: 100%;
  }
  & > button:first-child {
    border-right: none;
  }
`;

export const TradeForUser = ({book, acceptTradeRequset, cancelTradeRequest}) => (
  <BookWrapper>
    <Thumbnail book={book} />
    {book.accepted ? <Button disabled>Accepted</Button> :
      <ButtonsWrapper>
        <Button
          onClick={() => acceptTradeRequset(book.tradeId)}
          disabled={book.tradeButtonsStatus === 'inProcess'}
        >Accept</Button>
        <Button
          onClick={() => cancelTradeRequest(book.tradeId)}
          disabled={book.tradeButtonsStatus === 'inProcess'}
        >Cancel</Button>
      </ButtonsWrapper>
    }
  </BookWrapper>
);

export const TradesForUser = ({books, acceptTradeRequset, cancelTradeRequest}) => (
  <>
    <Title>Your books that others want:</Title>
    <BooksWrapper>
      {books.map(book => (
        <TradeForUser
          key={book.id}
          book={book}
          acceptTradeRequset={acceptTradeRequset}
          cancelTradeRequest={cancelTradeRequest}
        />
      ))}
    </BooksWrapper>
  </>
);

export const TradesForUserContainer = withRouter(connect(
  ({profile}) => ({books: profile.tradesForUser}),
  {acceptTradeRequset, cancelTradeRequest}
)(TradesForUser));

export const ProfileWrapper = styled.div`
  padding-top: .5rem;
  max-width: 1100px;
  margin: 0 auto;
`;

export class Profile extends React.Component {
  componentDidMount() {
    this.props.profileInitialStateHandler();
  }

  render() {
    const {error, loading} = this.props;

    if (error) {
      return <ProfileWrapper>{error}</ProfileWrapper>;
    }
  
    if (loading) {
      return <ProfileWrapper>Loading...</ProfileWrapper>
    }

    return (
      <ProfileWrapper>
        <UserInfoContainer />
        <UserTradesContainer />
        <TradesForUserContainer />
      </ProfileWrapper>
    );
  }
}

export const ProfileContainer = withRouter(connect(
  ({profile}) => ({...profile.initialState}),
  {profileInitialStateHandler}
)(Profile));

export default ProfileContainer;