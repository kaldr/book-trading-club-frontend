import React from 'react';
import {MemoryRouter} from 'react-router-dom';
import Enzyme, {mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import {Header} from './Header';

Enzyme.configure({adapter: new Adapter()});

function setup(username) {
	const wrapper = mount(
    <MemoryRouter>
      <Header username={username} />
    </MemoryRouter>
	);

	const header = wrapper.find(Header);
	return {
		header,
		h1: header.find('h1'),
		link: header.find('a'),
	};
}

describe('Header', () => {
	test('it always render the wrapping header and title', () => {
		const {header, h1} = setup();
		
		expect(header.find('header').length).toBe(1);
		expect(h1.length).toBe(1);
	});

	test('when no username, there is not a Link to profile', () => {
		const {header, link} = setup();

		expect(header.props().username).toBe(undefined);
		expect(link.length).toBe(0);
	});

	test('when username, there is a Link to profile', () => {
		const address = '/profile';
		const username = 'username';
		const {header, link} = setup(username);

		expect(header.props().username).toBe(username);
		expect(link.length).toBe(1);
		expect(link.prop('href')).toBe(address);
		expect(link.text()).toBe('Profile');
	});
});