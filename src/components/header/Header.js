import React from 'react';
import styled from 'styled-components';
import {Link, withRouter} from 'react-router-dom';
import {connect} from 'react-redux';

import SignOutButton from '../signin-up-out/SignOut';

const Div = styled.div`
  border-bottom: 1px solid #b7bac4;
`;

const Wrapper = styled.header`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 1em 0;
  margin: 0 auto;
  max-width: 1100px;
`;

const Title = styled.h1`
  font-size: 1.6em;
  margin: 0;
`;

const StyleLogoLink = styled(Link)`
  text-decoration: none;
  color: #151d3f;
`;

const StyledLink = styled(Link)`
  text-decoration: none;
  color: #151d3f;
  border: 1px solid #b7bac4;
  padding: .2rem .8rem;
  margin-right: .5rem;
  &:hover {
    background-color: rgb(240, 240, 240);
  }
`;

const ButtonsWrapper = styled.div`

`;

export const Header = ({username}) => (
  <Div>
    <Wrapper>
      <StyleLogoLink to="/">
        <Title>Book Trading Club</Title>
      </StyleLogoLink>
      {username &&
        <ButtonsWrapper>
          <StyledLink to="/profile">Profile</StyledLink>
          <SignOutButton />
        </ButtonsWrapper>
      }
    </Wrapper>
  </Div>
);

export const HeaderContainer = withRouter(connect(
  ({username}) => ({username})
)(Header));

export default HeaderContainer;