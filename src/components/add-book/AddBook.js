import React, {useState} from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';

import {Button} from '../books/Books';
import {
  textToSearchChangeHandling as handleChange,
  getSearchedBooksDebounced as getSearchedBooks,
  closeHandler,
  addBookHandler,
} from '../../actions/add-book/addBookActions';

export const NullThumbnail = styled.div`
  width: 128px;
  height: 192px;
  background-color: #eee;
`;

export function Thumbnail({book}) {
  const [isLoaded, setIsLoaded] = useState(false);

  if (!book.thumbnail) {
    return <NullThumbnail title={book.title} />;
  }

  return (
    <>
      <img
        style={!isLoaded ? {display: 'none'} : {}}
        onLoad={() => setIsLoaded(true)}
        src={book.thumbnail}
        alt={book.title}
        title={book.title}
      />
      {!isLoaded && <NullThumbnail title={book.title} />}
    </>
  );
};

const ThumbAndBtn = styled.div`
  display: inline-flex;
  flex-flow: column;
`;

const Title = styled.div`
  padding-left: .5rem;
`;

const BookWrapper = styled.div`
  display: flex;
  padding-bottom: .5rem;
  padding-left: .5rem;
`;

export const Book = ({book, addBookHandler}) => {
  const {addButtonState} = book;
  return (
    <BookWrapper>
      <ThumbAndBtn>
        <Thumbnail book={book} />
        <Button
          disabled={addButtonState !== 'waitForClick'}
          onClick={() => addBookHandler(book)}
        >
          {addButtonState === 'done' ? 'Done' : 'Add'}
        </Button>
      </ThumbAndBtn>
      <Title>{book.title}</Title>
    </BookWrapper>
  );
};

export const Books = ({books, addBookHandler}) =>
  books.map(book => <Book key={book.id} book={book} addBookHandler={addBookHandler} />);

export const BooksContainer = connect(state => ({books: state.addBookState.books}), {addBookHandler})(Books);

const SearchedBooksWrapper = styled.div`
  position: absolute;
  max-height: calc(100vh - 8rem);
  overflow-y: auto;
  min-width: 100%;
  background-color: #ffffff;
  border: 1px solid black;
  border-top: none;
  padding-top: .5rem;
`;

export const SearchedBooks = ({books, isSearching, error}) => {
  if (error) {
    return <SearchedBooksWrapper>{error}</SearchedBooksWrapper>;
  }

  if (isSearching) {
    return <SearchedBooksWrapper>Searching...</SearchedBooksWrapper>;
  }

  if (books.length === 0) {
    return null;
  }

  return (
    <SearchedBooksWrapper>
      <BooksContainer />
    </SearchedBooksWrapper>
  );
};

export const Input = styled.input`
  background-color: white;
  color: black;
  padding: .5rem;
  font-size: .8rem;
  border: 1px solid black;
  width: 15rem;
`;

const Wrapper = styled.div`
  font-size: .8rem;
  position: relative;
  display: inline-block;
`;

const Div = styled.div`
  display: flex;
`;

const CloseButton = styled.div`
  border: 1px solid black;
  font-size: .8rem;
  border-left: none;
  cursor: pointer;
  padding: .5rem;
`;

const AddBookWrapper = styled.div`
  max-width: 1100px;
  margin: 0 auto;
  padding-top: .5rem;
  padding-left: .5rem;
`;

export function AddBook({closeHandler, handleChange, addBookState}) {
  const {books, isSearching, error, textToSearch} = addBookState;
  return (
    <AddBookWrapper>
      <Wrapper>
        <Div>
          <Input
            onChange={event => handleChange(event.target.value, getSearchedBooks)}
            value={textToSearch}
            placeholder="Search and Add Books..."
          />
          {!error && !isSearching && books.length > 0 &&
            <CloseButton onClick={closeHandler}>Close</CloseButton>
          }
        </Div>
        <SearchedBooks {...addBookState} />
      </Wrapper>
    </AddBookWrapper>
  );
}

export const AddBookContainer = connect(state => ({
  addBookState: state.addBookState,
}), {handleChange, closeHandler})(AddBook);

export default AddBookContainer;