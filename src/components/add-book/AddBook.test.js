import React from 'react';
import {render, fireEvent, act} from '@testing-library/react';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import MockAdapter from 'axios-mock-adapter';

import {searchedBooksWithHelloPhrase} from '../../devUtils/mockApi';
import AddBook, * as components from './AddBook';
import reducer from '../../reducers/rootReducer';
import axios, {rawAxios} from '../../axios';

jest.mock('lodash/debounce', () => fn => fn);

const mockApi = new MockAdapter(axios);

function renderWithRedux(ui, initialState) {
  const store = createStore(reducer, initialState, applyMiddleware(thunk));
  return {
    ...render(<Provider store={store}>{ui}</Provider>),
    store,
  };
}

describe('thumbnail', () => {
  test('there is not thumbnail link: we use empty placeholder', () => {
    const book = {id: 'id', title: 'title', thumbnail: undefined};
    const {queryByAltText, getByTitle} = render(<components.Thumbnail book={book} />);    

    expect(queryByAltText(book.title)).toBe(null);
    expect(getByTitle(book.title)).toBeDefined();
  });

  test('there is thumbnail link: we use the thumbnail', () => {
    const book = {id: 'id', title: 'title', thumbnail: 'https://cdn.example.com/thumbnail'};
    const {getByAltText} = render(<components.Thumbnail book={book} />);

    expect(getByAltText(book.title)).toBeDefined();
    expect(getByAltText(book.title).src).toBe(book.thumbnail);
  });
});

describe('book', () => {
  test('add button status is waitForClick', () => {
    const book = {addButtonState: 'waitForClick', title: 'title'};
    const addBookHandler = jest.fn();
    const {getByText, getByTitle} = render(<components.Book book={book} addBookHandler={addBookHandler} />);
    expect(getByTitle(book.title)).toBeDefined();
    expect(getByText(book.title)).toBeDefined();
    expect(getByText('Add').disabled).toBe(false);

    fireEvent.click(getByText('Add'));
    expect(addBookHandler.mock.calls.length).toBe(1);
    expect(addBookHandler.mock.calls[0][0]).toEqual(book);
  });

  test('add button status is inProcess', () => {
    const book = {addButtonState: 'inProcess', title: 'title'};
    const addBookHandler = jest.fn();
    const {getByText, getByTitle} = render(<components.Book book={book} addBookHandler={addBookHandler} />);
    expect(getByTitle(book.title)).toBeDefined();
    expect(getByText(book.title)).toBeDefined();
    expect(getByText('Add').disabled).toBe(true);

    fireEvent.click(getByText('Add'));
    expect(addBookHandler.mock.calls.length).toBe(0);
  });

  test('add button status is done', () => {
    const book = {addButtonState: 'done', title: 'title'};
    const addBookHandler = jest.fn();
    const {getByText, getByTitle} = render(<components.Book book={book} addBookHandler={addBookHandler} />);
    expect(getByTitle(book.title)).toBeDefined();
    expect(getByText(book.title)).toBeDefined();
    expect(getByText('Done').disabled).toBe(true);

    fireEvent.click(getByText('Done'));
    expect(addBookHandler.mock.calls.length).toBe(0);
  });
});

describe('books', () => {
  test('renders some books, and you can add book', async () => {
    mockApi.onPost('/api/books').reply(200);
    const books = [
      {id: '1', title: 'title1', addButtonState: 'waitForClick'},
      {id: '2', title: 'title2', addButtonState: 'waitForClick'},
    ];
    const {getByTitle, getAllByText, findByText, store} = renderWithRedux(
      <components.BooksContainer />,
      {addBookState: {books}}
    );
    expect(getByTitle(books[0].title)).toBeDefined();
    expect(getByTitle(books[1].title)).toBeDefined();
    fireEvent.click(getAllByText('Add')[1]);
    expect(getAllByText('Add')[1].disabled).toBe(true);
    expect(await findByText('Done')).toBeDefined();
    const storeBooks = store.getState().books;
    expect(storeBooks[storeBooks.length - 1]).toEqual(books[1]);
  });
});

describe('searched books', () => {
  test('there is error', () => {
    const props = {
      error: 'error',
      isSearching: false,
      books: [{id: '1', title: 'title'}],
    };
    const {getByText, queryByText, queryByTitle} = renderWithRedux(<components.SearchedBooks {...props} />);
    expect(getByText(props.error)).toBeDefined();
    expect(queryByText('Searching...')).toBe(null);
    expect(queryByTitle(props.books[0].title)).toBe(null);
  });

  test('searching', () => {
    const props = {
      error: '',
      isSearching: true,
      books: [{id: '1', title: 'title'}],
    };
    const {getByText, queryByTitle} = renderWithRedux(<components.SearchedBooks {...props} />);
    expect(getByText('Searching...')).toBeDefined();
    expect(queryByTitle(props.books[0].title)).toBe(null);
  });

  test('there is no book', () => {
    const props = {
      error: '',
      isSearching: false,
      books: [],
    };
    const {queryByText, container} = renderWithRedux(<components.SearchedBooks {...props} />);
    expect(queryByText('Searching...')).toBe(null);
    expect(container.textContent).toBe('');
  });

  test('there books', () => {
    const props = {
      error: '',
      isSearching: false,
      books: [{id: '1', title: 'title'}],
    };
    const {queryByText, getByTitle} = renderWithRedux(
      <components.SearchedBooks {...props} />,
      {addBookState: {books: props.books}}
    );
    expect(queryByText('Searching...')).toBe(null);
    expect(getByTitle(props.books[0].title)).toBeDefined();
  });
});

describe('add book', () => {
  test('it should render', async () => {
    const books = [{"id":"fP-mCwAAQBAJ","title":"Hello!","thumbnail":"https://books.google.com/books/content?id=fP-mCwAAQBAJ&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api","owner":"username","addButtonState":"waitForClick"},{"id":"pwRcrgEACAAJ","title":"Hello?","thumbnail":"https://books.google.com/books/content?id=pwRcrgEACAAJ&printsec=frontcover&img=1&zoom=1&source=gbs_api","owner":"username","addButtonState":"waitForClick"},{"id":"AKQd0WfQaw0C","title":"Hello, Hello Brazil","thumbnail":"https://books.google.com/books/content?id=AKQd0WfQaw0C&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api","owner":"username","addButtonState":"waitForClick"},{"id":"3u3WwWg82nQC","title":"Hello! Good-bye!","thumbnail":"https://books.google.com/books/content?id=3u3WwWg82nQC&printsec=frontcover&img=1&zoom=1&source=gbs_api","owner":"username","addButtonState":"waitForClick"},{"id":"AhBNeNVzMGIC","title":"Hello","thumbnail":"https://books.google.com/books/content?id=AhBNeNVzMGIC&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api","owner":"username","addButtonState":"waitForClick"}];
    const mockRawAxios = new MockAdapter(rawAxios);
    searchedBooksWithHelloPhrase(mockRawAxios);
    const {getByPlaceholderText, findByText, queryByText, getByText} = renderWithRedux(<AddBook />);
    expect(getByPlaceholderText('Search and Add Books...')).toBeDefined();
    expect(queryByText('Searching...')).toBe(null);
    fireEvent.change(getByPlaceholderText('Search and Add Books...'), {target: {value: 'search text'}});
    expect(getByText('Searching...')).toBeDefined();
    expect(queryByText('Close')).toBe(null);
    expect(await findByText(books[0].title)).toBeDefined();
    expect(getByText('Close')).toBeDefined();
    fireEvent.click(getByText('Close'));
    expect(getByPlaceholderText('Search and Add Books...').value).toBe('');
    expect(queryByText(books[0].title)).toBe(null);
    mockRawAxios.reset();
  });
});