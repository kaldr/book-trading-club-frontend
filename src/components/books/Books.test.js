import React from 'react';
import Enzyme, {mount, shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import * as components from './Books';
import {Thumbnail} from '../add-book/AddBook';

Enzyme.configure({adapter: new Adapter()});

describe('Book trade button', () => {
  function setup(props) {
    return shallow(<components.Book {...props} />);
  }

  test('there is no username (anonymous user) or user is owner, trade button is disabled', () => {
    // anonymous user
    const props = {
      username: '',
      book: {id: 1, tradeAccepted: null},
      trades: {tradeStatus: {}},
    };
    const Book = setup(props);
    const button = Book.find(components.Button);
    expect(button.prop('disabled')).toBe(true);
  });

  test('user is book owner, trade button is disabled', () => {
    const props = {
      username: 'user',
      book: {id: 1, tradeAccepted: null, owner: 'user'},
      trades: {tradeStatus: {}},
    };
    const Book = setup(props);
    const button = Book.find(components.Button);
    expect(button.prop('disabled')).toBe(true);
  });

  test('trade request status is inProcess, button is disabled', () => {
    const props = {
      username: 'user',
      book: {id: 1, tradeAccepted: null, owner: 'anotherUser'},
      trades: {tradeStatus: {'1': 'inProcess'}},
    };
    const Book = setup(props);
    const button = Book.find(components.Button);
    expect(button.prop('disabled')).toBe(true);
  });

  test('trade request status is done, button is disabled', () => {
    const props = {
      username: 'user',
      book: {id: 1, tradeAccepted: null, owner: 'anotherUser'},
      trades: {tradeStatus: {'1': 'done'}},
    };
    const Book = setup(props);
    const button = Book.find(components.Button);
    expect(button.prop('disabled')).toBe(true);
  });

  test('tradeAccepted is not null (it is true or false), button is disabled', () => {
    const props = {
      username: 'user',
      book: {id: 1, tradeAccepted: false, owner: 'anotherUser'},
      trades: {tradeStatus: {}},
    };
    const Book = setup(props);
    const button = Book.find(components.Button);
    expect(button.prop('disabled')).toBe(true);
  });

  test('trade status done, trade button text is Done', () => {
    const props = {
      username: '',
      book: {id: 1},
      trades: {tradeStatus: {'1': 'done'}},
    };
    const Book = setup(props);
    const button = Book.find(components.Button);
    expect(button.text()).toBe('Done');
  });

  test('trade status !== done, trade button text is Trade', () => {
    const props = {
      username: '',
      book: {id: 1},
      trades: {tradeStatus: {'1': 'not done'}},
    };
    const Book = setup(props);
    const button = Book.find(components.Button);
    expect(button.text()).toBe('Trade');
  });

  test('sub components and clicking', () => {
    const props = {
      username: 'user',
      book: {id: 1, tradeAccepted: null, owner: 'anotherUser'},
      trades: {tradeStatus: {}},
      tradeHandler: jest.fn(),
    };
    const Book = setup(props);
    const button = Book.find(components.Button);
    const thumbnail = Book.find(Thumbnail);

    expect(button.length).toBe(1);
    expect(thumbnail.length).toBe(1);
    button.simulate('click');
    expect(props.tradeHandler.mock.calls.length).toBe(1);
  });
});

describe('books', () => {
  test('it should render multiple Book', () => {
    const books = [{id: '1', userBookId: '1'}, {id: '2', userBookId: '2'}];
    const Books = mount(<components.Books books={books} trades={{tradeStatus: {}}} />);
    expect(Books.find(components.Book).length).toBe(2);
  });
});

describe('books holder', () => {
  test('it should render', () => {
    const BooksHolder = shallow(<components.BooksHolder />);
    expect(BooksHolder.find(components.BooksContainer).length).toBe(1);
  });
});