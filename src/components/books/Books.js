import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';

import {tradeHandler} from '../../actions/books/booksActions';
import {Thumbnail} from '../add-book/AddBook';

export const BookWrapper = styled.div`
  display: flex;
  flex-flow: column;
  margin: 0 .5em;
  padding-bottom: .5em;
`;

export const BooksWrapper = styled.div`
  display: flex;
  flex-flow: row wrap;
  align-items: flex-end;
  padding-top: .5rem;
  max-width: 1100px;
  margin: 0 auto;
`;

export const Button = styled.button`
  min-width: 3.5rem;
  background-color: white;
  color: ${props => props.disabled ? 'rgb(160, 160, 160)' : 'black'};
  border: 1px solid ${props => props.disabled ? 'rgb(160, 160, 160)' : 'black'};
  padding: .5em;
  cursor: ${props => props.disabled ? 'default' : 'pointer'};
  &:hover {
    background-color: ${props => props.disabled ? 'white' : 'rgb(240, 240, 240)'};
  }
`;

export const Book = ({book, username, trades, tradeHandler}) => {
  const tradeStatus = trades.tradeStatus[book.id];
  const disabled = !username || tradeStatus === 'inProcess' ||
    tradeStatus === 'done' || username === book.owner || book.tradeAccepted !== null;
  
  let buttonText = 'Trade';
  if (tradeStatus === 'done') {
    buttonText = 'Done';
  }
  
  return (
    <BookWrapper>
      <Thumbnail book={book} />
      <Button
        disabled={disabled}
        onClick={() => tradeHandler(book)}
      >
        {buttonText}
      </Button>
    </BookWrapper>
  );
};

export const Books = ({books, username, trades, tradeHandler}) => books.map(book =>
  <Book
    key={book.userBookId}
    book={book}
    username={username}
    trades={trades}
    tradeHandler={tradeHandler}
  />);

export const BooksContainer = connect(state => ({
  books: state.books,
  username: state.username,
  trades: state.trades,
}), {tradeHandler})(Books);

export const BooksHolder = () => (
  <BooksWrapper>
    <BooksContainer />
  </BooksWrapper>
);

export default BooksHolder;