import axios from 'axios';

export const rawAxios = axios.create();

axios.interceptors.request.use(request => {
  const authHeader = localStorage.getItem('authHeader') || '';
  request.headers['Authorization'] = authHeader;
  return request;
});

axios.interceptors.response.use(response => {
  const authHeader = response.headers ? response.headers['authorization'] : undefined;
  if (authHeader) {
    localStorage.setItem('authHeader', authHeader);
  }
  return response;
});

export default axios;